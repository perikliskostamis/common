import { Input, EventEmitter, Component } from '@angular/core';
import { RouterModal } from './RouterModal';
import {ButtonType, ButtonTypes} from './ButtonTypes';

export abstract class RouterModalAbortRetryIgnore extends RouterModal {

    public readonly buttonChanges = new EventEmitter<{ [ button: string]: ButtonType }>();

    protected get abortButtonState(): { abort: ButtonType } {
        return {
            abort: {
                buttonText: this._abortButtonText,
                buttonClass: this._abortButtonClass,
                buttonDisabled: this._abortButtonDisabled
            }
        };
    }
    protected get retryButtonState(): { retry: ButtonType } {
        return {
            retry: {
                buttonText: this._retryButtonText,
                buttonClass: this._retryButtonClass,
                buttonDisabled: this._retryButtonDisabled
            }
        };
    }
    protected get ignoreButtonState(): { ignore: ButtonType } {
        return {
            ignore: {
                buttonText: this._ignoreButtonText,
                buttonClass: this._ignoreButtonClass,
                buttonDisabled: this._ignoreButtonDisabled
            }
        };
    }

    // abort button (start)
    private _abortButtonText = ButtonTypes.abort.buttonText;
    @Input()
    public get abortButtonText() {
        return this._abortButtonText;
    }
    public set abortButtonText(value) {
        this._abortButtonText = value;
        this.buttonChanges.emit(this.abortButtonState);
    }
    private _abortButtonClass = ButtonTypes.abort.buttonClass;
    @Input()
    public get abortButtonClass() {
        return this._abortButtonClass;
    }
    public set abortButtonClass(value) {
        this._abortButtonClass = value;
        this.buttonChanges.emit(this.abortButtonState);
    }
    private _abortButtonDisabled = ButtonTypes.abort.buttonDisabled;
    @Input()
    public get abortButtonDisabled() {
        return this._abortButtonDisabled;
    }
    public set abortButtonDisabled(value) {
        this._abortButtonDisabled = value;
        this.buttonChanges.emit(this.abortButtonState);
    }
    // abort button (end)

    // retry button (start)
    private _retryButtonText = ButtonTypes.retry.buttonText;
    @Input()
    public get retryButtonText() {
        return this._retryButtonText;
    }
    public set retryButtonText(value) {
        this._retryButtonText = value;
        this.buttonChanges.emit(this.retryButtonState);
    }
    private _retryButtonClass = ButtonTypes.retry.buttonClass;
    @Input()
    public get retryButtonClass() {
        return this._retryButtonClass;
    }
    public set retryButtonClass(value) {
        this._retryButtonClass = value;
        this.buttonChanges.emit(this.retryButtonState);
    }
    private _retryButtonDisabled = ButtonTypes.retry.buttonDisabled;
    @Input()
    public get retryButtonDisabled() {
        return this._retryButtonDisabled;
    }
    public set retryButtonDisabled(value) {
        this._retryButtonDisabled = value;
        this.buttonChanges.emit(this.retryButtonState);
    }
    // retry button (end)

    // ignore button (start)
    private _ignoreButtonText = ButtonTypes.ignore.buttonText;
    @Input()
    public get ignoreButtonText() {
        return this._ignoreButtonText;
    }
    public set ignoreButtonText(value) {
        this._ignoreButtonText = value;
        this.buttonChanges.emit(this.ignoreButtonState);
    }
    private _ignoreButtonClass = ButtonTypes.ignore.buttonClass;
    @Input()
    public get ignoreButtonClass() {
        return this._ignoreButtonClass;
    }
    public set ignoreButtonClass(value) {
        this._ignoreButtonClass = value;
        this.buttonChanges.emit(this.ignoreButtonState);
    }
    private _ignoreButtonDisabled = ButtonTypes.ignore.buttonDisabled;
    @Input()
    public get ignoreButtonDisabled() {
        return this._ignoreButtonDisabled;
    }
    public set ignoreButtonDisabled(value) {
        this._ignoreButtonDisabled = value;
        this.buttonChanges.emit(this.ignoreButtonState);
    }
    // ignore button (end)

    // noinspection JSUnusedGlobalSymbols
    abstract abort(): Promise<any>;
    abstract retry(): Promise<any>;
    abstract ignore(): Promise<any>;
}
