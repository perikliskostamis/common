import { UserActivityService, UserActivityEntry } from './../user-activity/user-activity.service';

/**
 *
 * SessionUserActivityService
 *
 * Handles the io operations with the session storage for the user activity
 * service
 *
 */

export class SessionUserActivityService extends UserActivityService {

  constructor() {
    super();
    const userActivity = sessionStorage.getItem('userActivity');

    if (userActivity) {
      const rawList = JSON.parse(userActivity);

      // NOTE: JSON.stringify transforms dates as ISOString, but the
      //       UserActivityEntry expects Date object.
      this.list = rawList.map((item) => ({
          ...item,
          dateCreated: new Date(item.dateCreated)
        })
      );
    }
  }

  /**
   *
   * Adds a new entry at the user activity list
   *
   * @param {UserActivityEntry} entry The entry to write at the session storage
   *
   * @override
   *
   */
  public setItem(entry: UserActivityEntry): Promise<void> {
    return super.setItem(entry).then(() => {
      sessionStorage.setItem('userActivity', JSON.stringify(this.list));
    });
  }
}
