import { Component, Input, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { Subscription } from 'rxjs';
import { ActivatedUser } from '../../services/activated-user.service';
import { AuthenticationService, OnRefreshToken } from '../../services/authentication.service';

@Component({
    selector: 'universis-refresh-token',
    template: `<div></div>`,
    encapsulation: ViewEncapsulation.None,
})

export class RefreshTokenComponent implements OnInit, OnDestroy {

    private userSubscription: Subscription;
    private refreshTimer: any;
    private refreshErrors = 0;

    /**
     * Gets or sets the validation interval. The default value is 60000 ms.
     */
    @Input() timerInterval = 60000;
    /**
     * Sets the number of milliseconds -before expiration- for refreshing an access token.
     * The default value is 60000 ms.
     */
    @Input() refreshBefore = 60000;

    constructor(private activatedUser: ActivatedUser,
        private authService: AuthenticationService,
        private context: AngularDataContext) {
    }
    ngOnDestroy(): void {
        if (this.userSubscription) {
            this.userSubscription.unsubscribe();
        }
        this.resetTimer();
    }

    private resetTimer() {
        if (this.refreshTimer) {
            clearInterval(this.refreshTimer);
            this.refreshTimer = null;
        }
    }

    ngOnInit(): void {
        this.userSubscription = this.activatedUser.user.subscribe((user) => {
            if (user) {
                // reset timer
                this.resetTimer();
                const token: {
                    expires_in?: number;
                    refresh_token?: string;
                    refresh_expires_in?: number;
                    created_at?: Date;
                } = user.token;
                if (token && token.refresh_token) {
                    // get expiration
                    const expires_in = token.expires_in;
                    if (Number.isInteger(expires_in) && expires_in > 0) {
                        if (token.created_at !== null) {
                            // get date created
                            const createdAt = new Date(token.created_at);
                            // get expiration date and time
                            const willBeExpiredAt = new Date(createdAt.getTime() + expires_in * 1000);
                            this.refreshTimer = setInterval(() => {
                                if (new Date().getTime() + this.refreshBefore >= willBeExpiredAt.getTime()) {
                                    // do refresh
                                    const service = this.authService as any as OnRefreshToken;
                                    if (typeof service.refresh === 'function') {
                                        service.refresh().then((result: any) => {
                                            // clone result and refresh token
                                            user.token = JSON.parse(JSON.stringify(Object.assign(result, {
                                                created_at: new Date()
                                            })));
                                            // clear interval
                                            this.resetTimer();
                                            // store user to storage
                                            sessionStorage.setItem('currentUser', JSON.stringify(user));
                                            // set bearer authorization
                                            this.context.setBearerAuthorization(user.token.access_token);
                                            // and notify components for change
                                            this.activatedUser.user.next(user);
                                        }).catch((err) => {
                                            this.refreshErrors += 1;
                                            if (this.refreshErrors > 3) {
                                                return;
                                            }
                                            console.error('An error occurred while trying to refresh token');
                                            console.error(err);
                                        });
                                    }
                                }
                            }, this.timerInterval);
                        } else {
                            console.warn('REFRESH_TOKEN', 'Refresh token operation cannot be completed' +
                                ' because token timestamp cannot be determined.');
                        }
                    } else {
                        console.warn('REFRESH_TOKEN', 'Refresh token operation cannot be completed' +
                            ' because expiration timeout has not been set.');
                    }
                }
            } else {
                // clear interval because user is null
                this.resetTimer();
            }
        });
    }
}
